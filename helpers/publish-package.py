#!/usr/bin/python3
import os
import sys
import tarfile
import tempfile
import argparse
from helperslib import Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to publish a package into an archive.')
parser.add_argument('--package', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--environment', type=str, required=True)
arguments = parser.parse_args()

# Initialize the archive manager and ask it to publish the package
# This will also cause the Manifest for the Archive to be regenerated
ourArchive = Packages.Archive( arguments.environment, arguments.platform, usingCache = False )
ourArchive.publishPackage( arguments.package )

